import React from 'react';
import { TouchableOpacity, Text, StyleSheet, View } from 'react-native';
import {COLORS} from '../constants/Colors.js';
import {RowItem} from './RowItem.js';

export default function CustomComponents() {
    return (
        <View >
            <RowItem
                text="Themes"/>
            <RowItem
                text="React native basics"/>
            <RowItem
                text="React native by example"/>               
        </View>

    )
}
