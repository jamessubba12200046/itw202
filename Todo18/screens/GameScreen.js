import { StyleSheet, Text, View, Alert, FlatList } from 'react-native'
import React, {useState, useEffect} from 'react'
import {useWindowDimensions} from 'react-native'
import Title from '../components/ui/Title'
import NumberContainer from '../components/Game/NumberContainer';
import PrimaryButton from '../components/ui/PrimaryButton';
import InstructionText from '../components/ui/InstructionText';
import Card from '../components/ui/Card';
import GuessLogItem from '../components/Game/GuessLogItem';
import {Ionicons} from '@expo/vector-icons'

function generateRandomNumber(min, max, exclude){
  const rndNum=Math.floor(Math.random()*(max-min))+min;
  if(rndNum==exclude){
    return generateRandomNumber(min, max, exclude) 
  }
  else{
    return rndNum;
  }
}

let minBoundary=1;
let maxBoundary=100;

const GameScreen = ({userNumber, onGameOver}) => {
  const initialGuess = generateRandomNumber(1, 100, userNumber)
  const[currentGuess, setCurrentGuess]=useState(initialGuess)
  const[guessRound, setGuessRound]=useState([initialGuess])
  const {width, height} = useWindowDimensions();

  useEffect (() => {
    if(currentGuess === userNumber){
      onGameOver(guessRound.length);
    }
  },[currentGuess, userNumber, onGameOver])

  useEffect (() => {
   minBoundary=1;
   maxBoundary=100;
  },[])


  function nextGuessHandler(direction){
    if(
      (direction==='lower' && currentGuess < userNumber) ||
      (direction==='greater' && currentGuess > userNumber)
    ){
      Alert.alert("Dont lie!", "You know that this wrong...",[
        {text: "Sorry", style: "cancel"}

      ])
      return;
    }
    if(direction=='lower'){
      maxBoundary = currentGuess;
    }
    else{
      minBoundary = currentGuess+1;
    }
    const newRndNumber = generateRandomNumber(minBoundary, maxBoundary, currentGuess)
    setCurrentGuess(newRndNumber);
    setGuessRound((prevGuessRound=>[newRndNumber, ...prevGuessRound]))
  }

  const guessRoundListLength=guessRound.length

  let content =(
    <>
      <Card>
        <NumberContainer>{currentGuess}</NumberContainer>
          <InstructionText style={styles.instructionText}>Higher or Lower</InstructionText>
          <View style={styles.buttonContainer}>
            <View style={styles.buttonContainer}>
              <PrimaryButton onPress={nextGuessHandler.bind(this, 'greater')}>
                <Ionicons name='md-add' size={24} color='white'/>
              </PrimaryButton>
            </View>
            
            <View style={styles.buttonContainer}>
              <PrimaryButton onPress={nextGuessHandler.bind(this, 'lower')}>
                <Ionicons name='md-remove' size={24} color='white'/>
              </PrimaryButton>
            </View>
          </View>
      </Card>
    </>
  );
  if (width > 500){
    content =(
      <>
       <InstructionText style={styles.instructionText}>Higher or Lower</InstructionText>
          <View style={styles.ButtonContainerWide}>
            <View style={styles.buttonContainer}>
              <PrimaryButton onPress={nextGuessHandler.bind(this, 'greater')}>
                <Ionicons name='md-add' size={24} color='white'/>
              </PrimaryButton>
            </View>
            <NumberContainer>{currentGuess}</NumberContainer>
            <View style={styles.buttonContainer}>
              <PrimaryButton onPress={nextGuessHandler.bind(this, 'lower')}>
                <Ionicons name='md-remove' size={24} color='white'/>
              </PrimaryButton>
            </View>
          </View>
      </>
    )
  }


  return (
    <View style={styles.screen}>
    <Title>Opponent's Guess</Title>
    
    {content}

      <View style={styles.listcontainer}>

        <FlatList
        data={guessRound}
        renderItem={(itemData)=>
          <GuessLogItem 
          roundNumber={guessRoundListLength - itemData.index}
          guess={itemData.item}/>
        }
        keyExtractor={(item)=>item}
        />   

      </View>
    </View>
  )
}

export default GameScreen

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    padding: 24,
    alignItems:'center'
  },
  buttonsContainer: {
    flexDirection:'row'
  },
  buttonContainer:{
    flex: 1,
  },
  instructionText:{
    marginBottom: 12
  },

  ButtonContainerWide:{
    flexDirection:'row',
    alignItems:'center'
  },
   listcontainer:{
     flex:1,
    //  padding:12

   }
 
})