import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { add, multiply } from './Components/nameExport';
import division from './Components/defaultExport';

export default function App() {
  return (
    <View style={styles.container}>
      <Text>hi </Text>
      <Text>Result of Additionn: {add(5,6)}</Text>
      <Text>Result of Multiplication: {multiply(5, 8)}</Text>
      <Text>Result of division: {division(10,2)}</Text>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
