
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  const name = "James Subba";
  return (
    <View style={styles.container}>
      <Text style={styles.text1}>Getting Started with React Native!</Text>
      <Text style={styles.text2}>My name is {name}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text1: {
    fontWeight: 'bold',
    fontSize: 45,
  },
  text2: {
    fontSize: 20,
  }

});
