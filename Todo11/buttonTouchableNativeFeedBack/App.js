import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import TouchableNativeFeedbackExample from './component/TouchableNativeFeedbackExample';
export default function App() {
  return (
    <View style={styles.container}>
        <TouchableNativeFeedbackExample/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
