import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <View style={styles.red}><Text>1</Text></View>
      <View style={styles.blue}><Text>2</Text></View>
      <View style={styles.green}><Text>3</Text></View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'start',
    justifyContent: 'center',
    flexDirection: 'row',
    padding: 40,
    width: 300,
  },
  red: {
    backgroundColor: 'red',
    height: 200,
    width: 100,
    flex: 5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  blue: {
    backgroundColor: 'blue',
    height: 200,
    width: 100,
    flex: 10,
    justifyContent: 'center',
    alignItems: 'center'

  },
  green: {
    backgroundColor: 'green',
    height: 200,
    width: 100,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }

});
