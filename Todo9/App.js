import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <Image
        style={styles.img1}
        source={{uri: 'https://i.picsum.photos/id/603/100/100.jpg?hmac=HQC6_xw6ed5VLUP_eLAf0W5DfNSrP2yzTP_3RD0s0oc'}}
        />
        <Image
        style={styles.img2}
        source={require('./assets/react-logo.png')}/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  img1: {
    height: 100,
    width: 100,
    resizeMode: 'stretch'
  },
  img2: {
    height: 100,
    width: 100, 
    resizeMode: 'contain'
  }
});
