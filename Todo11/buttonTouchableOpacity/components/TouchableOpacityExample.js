import React, {useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity } from 'react-native';

const TouchableOpacityExample = () => {
    const [count, setCount] = useState(0);
    return (
        <View style={{padding: 100}}>
            <Text>You clicked {count} times</Text>
            <TouchableOpacity style={styles.button}
            onPress={() => setCount(count + 1)}>
                <Text>Count</Text>
            </TouchableOpacity>
        </View>
    )
}
const styles = StyleSheet.create({
    button: {
        alignItems: "center",
        backgroundColor: "#DDDDDD",
        padding: 10,
    }
});
export default TouchableOpacityExample;