import React, {useState} from 'react';
import {TextInput, View} from 'react-native';

const TextInputComponent = () => {
    const [text, setText] = useState('');
    return (
        <View>
            <TextInput 
                placeholder='Enter the text here'
                style={{borderColor: 'yellow', borderWidth: 3}}
                multiline={false}
                onChangeText={text => setText(text.toLowerCase())}
            />
        </View>
    )
}

export default TextInputComponent;