import { StyleSheet, Text, View, ScrollView, Image, TextInput, Button } from 'react-native';

export default function App() {
  return (
    <ScrollView>
      <Text>Some Text</Text>
      <View>
        <Text>Some more text</Text>
        <Image
          style={styles.img}
          source={{
            uri: "https://picsum.photos/64/64",
          }}
          />
      </View>
      <TextInput
        defaultValue="You can type here"
        />
      <Button 
        onPress={() => {
          alert('You tapped the button');
        }}
        title="Press Me"
        />
        
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  img: {
    height: 100,
    width: 100,
    resizeMode: 'strech'
  }
});
