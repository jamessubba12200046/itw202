import { StyleSheet, Text, View } from 'react-native';
import TouchableHighlightExample from './components/TouchableHighlightExample';
export default function App() {
  return (
    <View style={styles.container}>
        <TouchableHighlightExample/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    
  },
});
