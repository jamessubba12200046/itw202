import React from 'react';
import {View,Text,StyleSheet} from 'react-native';


const MyComponent = () => {
    const greeting='Sonam Wangmo';
    const greetings=<Text>Jigme Wangmo</Text>
    return (
        <View>
            <Text style = {styles.textStyles}>This is the Demo of JSX</Text>
            <Text>Hi there!!!{greeting}</Text>
            {greetings}
        </View>
    )

}
const styles = StyleSheet.create({
    textStyles: {
        fontSize: 24,
    }

})
export default MyComponent;